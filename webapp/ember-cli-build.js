/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

/* Custom includes */
var Funnel = require('broccoli-funnel');
var BroccoliMergeTrees = require('broccoli-merge-trees');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here

    lessOptions: {
      paths: [
        'bower_components/uikit/src/less'
      ]
    },

    fingerprint: {
      exclude: ['assets/logo-bt.png']
    }

  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.
  //
  // Funnel will include all files in the folder
  // maybe include the "include: ['**/*.*']," option to do recursive (untested)


  // Include the next two if not building uikit from source
  app.import(app.bowerDirectory + '/uikit/dist/js/uikit.js');
  // app.import(app.bowerDirectory + '/uikit/dist/css/uikit.css');
  // app.import(app.bowerDirectory + '/uikit/dist/js/uikit.js', {
    // using: [
    //   { transformation: 'transpile-es2015', as: 'uikit', module_root: app.bowerDirectory + '/uikit/dist/' }
    // ]
    // uikit: 'default'
  // });
  // app.import(app.bowerDirectory + '/uikit/src/js/uikit.js');
  // var uikitImages = new Funnel(app.bowerDirectory + '/uikit/dist/images', { destDir: '/images' });
  // var uikitImages2 = new Funnel(app.bowerDirectory + '/uikit/dist/images', { destDir: '/dist/images' });
  var uikitImages = new Funnel(app.bowerDirectory + '/uikit/src/images', { destDir: '/images' });
  // app.import()

  app.import(app.bowerDirectory + '/trumbowyg/dist/trumbowyg.js');
  app.import(app.bowerDirectory + '/trumbowyg/dist/ui/trumbowyg.css');
  app.import(app.bowerDirectory + '/trumbowyg/dist/ui/icons.svg', { destDir: '/assets/ui' });

  app.import(app.bowerDirectory + '/material-design-icons/iconfont/material-icons.css');
  var icons = new Funnel(app.bowerDirectory + '/material-design-icons/iconfont/', { destDir: '/assets' });

  // app.import(app.bowerDirectory + '/font-awesome/css/font-awesome.css');
  // var fontAwesomeFonts = new Funnel(app.bowerDirectory + '/font-awesome/fonts', { destDir: '/fonts' });

  // Don't need to do this, because it's included with ember install ember-cli-chartjs
  app.import(app.bowerDirectory + '/chartjs/dist/Chart.bundle.js');

  app.import('vendor/javascript-md5/md5.js');

  app.import('vendor/scratchpad-standalone.min.js');
  app.import('vendor/btScratchPadify.js');

  // This doesn't work.  App.import doesn't know where node_modules folder is.
  // app.import('node_modules/colors.css/css/colors.css');
  // app.import('node_modules/colors.css/js/colors.js');

  app.import('vendor/animate.css');
  app.import('vendor/jquery.lettering.js');
  app.import('vendor/jquery.textillate.js');
  app.import('vendor/jquery.confetti.js/jquery.confetti.js');

  // return app.toTree(); // default
  // return new BroccoliMergeTrees([app.toTree()]); // supports merging multiple
  return new BroccoliMergeTrees([app.toTree(), uikitImages, icons]);
};
