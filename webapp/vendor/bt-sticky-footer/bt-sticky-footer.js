function btSetMinHeight() {
  $('.main-content').css('min-height', $(window).outerHeight() - $('header').outerHeight() - $('footer').outerHeight() - $('.main-content').outerHeight() + $('.main-content').height());
}

window.addEventListener("load", function () {
  $(document).ready(function() {
    // Do it several times to ensure it applies asap

    btSetMinHeight();

    setTimeout(function() {
      btSetMinHeight();
    }, 200);

    setTimeout(function() {
      btSetMinHeight();
    }, 400);

    setTimeout(function() {
      btSetMinHeight();
    }, 600);

    setTimeout(function() {
      btSetMinHeight();
    }, 1000);

    // Also do it on window resize
    $(window).resize(function() {
      $('.main-content').css('min-height', $(window).outerHeight() - $('header').outerHeight() - $('footer').outerHeight() - $('.main-content').outerHeight() + $('.main-content').height());
    });

  });
});
