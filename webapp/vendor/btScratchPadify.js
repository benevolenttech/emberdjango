
// Example: btScratchPadify('scratchpad2', 'scratch me!', 14, '#555', '#bbb', 50, 0.95, function() {});

// Bugs: scratcherThickness center gets off if you change it from default.

function btScratchPadify(eid, text, heightOffset, backgroundColor, stripeColor, scratcherThickness, progressComplete, completedCallback) {

  // Retrieve the target element
  var element = document.getElementById(eid);

  // Build the painter
  var painter = new Scratchcard.Painter({thickness: scratcherThickness});

  painter.reset = function reset(ctx, width, height) {


    ctx.fillStyle = backgroundColor;
    ctx.globalCompositeOperation = 'source-over';
    ctx.fillRect(0, 0, width, height);

    if (stripeColor) {
      for (var i = 0; i < 20; i++) {
        ctx.beginPath();
        ctx.moveTo(0, 40 * i);
        ctx.lineTo(2000, 40 * i - 1000);
        ctx.lineWidth = 18;
        ctx.strokeStyle = stripeColor;
        ctx.stroke();
      }
    }

    ctx.font = '70px Sacramento, Cursive';
    ctx.fillStyle = 'white';
    ctx.textAlign = 'center';
    ctx.fillText(text, parseInt(width/2), parseInt(height/2 + heightOffset));

  };

  // Build the scratchcard
  var scratchcard;
  try {
    scratchcard = new Scratchcard(element, {
      realtime: true,
      painter: painter
    });
  } catch (error) {
    alert(error);
    throw error;
  }

  // Listen for progress events
  scratchcard.on('progress', function onProgress(progress) {
    // console.log('Progress:', progress);
    if ((progress > progressComplete) && (progress < 1)) {
      scratchcard.complete();
      completedCallback();
    }
  });

  // Expose the scratchcard instance for debug/tests
  window.scratchcard = scratchcard;
}