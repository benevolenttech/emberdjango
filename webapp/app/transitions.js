
export default function() {
  /* Tips
   - this.reverse doesn't work on overrides when setting a default
   - when multiple rules match, the last defined one is used allowing
   for default
   */

  // Set default to fade.
  this.transition(
    this.fromRoute(function (route) {
      return true;
    }),
    this.toRoute(function (route) {
      return true;
    }),
    this.use('fade')
    // this.reverse('fade')
  );

  // Todo and register
  this.transition(
    this.fromRoute(function (route) {
      return true;
    }),
    this.toRoute(function (route) {
      return ['login', 'register'].indexOf(route) !== -1;
    }),
    // this.use('toDown', {duration: 600})
    this.use('drawer', 'y', 1, 'open')
  );
  this.transition(
    this.toRoute(function (route) {
      return true;
    }),
    this.fromRoute(function (route) {
      return ['login', 'register', 'register-complete'].indexOf(route) !== -1;
    }),
    // this.use('toUp', {duration: 600})
    this.use('drawer', 'y', -1, 'close')
  );
  this.transition(
    this.fromRoute('login'),
    this.toRoute('register'),
    this.use('toLeft', {duration: 600})
  );
  this.transition(
    this.fromRoute('register'),
    this.toRoute('login'),
    this.use('toRight', {duration: 600})
  );

  // To and from TODOs
  this.transition(
    this.fromRoute(function (route) {
      return true;
    }),
    this.toRoute('todo-entry'),
    // this.use('toUp', {duration: 700}, "ease")
    this.use('drawer', 'y', -1, 'open')
  );
  this.transition(
    this.fromRoute('todo-entry'),
    this.toRoute(function (route) {
      return true;
    }),
    // this.use('toDown', {duration: 1100}, "ease")
    this.use('drawer', 'y', 1, 'close', {duration: 1100})
  );


  // Home splash transitions
  this.transition(
    this.fromRoute('layout-info.index'),
    this.toRoute('layout-info.benefit1'),
    this.use('toLeft', {duration: 400}, "ease")
  );
  this.transition(
    this.fromRoute('layout-info.benefit1'),
    this.toRoute('layout-info.benefit2'),
    this.use('toUp', {duration: 400}, "ease")
  );
  this.transition(
    this.fromRoute('layout-info.benefit2'),
    this.toRoute('layout-info.connect'),
    this.use('toRight', {duration: 400}, "ease")
  );
}