import Ember from 'ember';

export default Ember.Route.extend({
  /* Hint: Reset action is in the login controller... */

  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Benevolent Tech | Password Reset");
  },
});
