import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

export default Ember.Route.extend(UnauthenticatedRouteMixin, {
  /* Hint: Login action is in the login controller... */

  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Benevolent Tech | Login");
  },

  actions: {
    // Hook entering a route.
    didTransition: function () {
      console.log("Doing");
        Ember.run.scheduleOnce('afterRender', this, function() {
          $('.login-form input').first().focus();
          setTimeout(function() { $('.login-form input').first().focus(); }, 600);
          setTimeout(function() { $('.login-form input').first().focus(); }, 1000);
        });
    },
  }
});