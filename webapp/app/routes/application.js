import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
// import UIkit from 'bower_components/uikit/src/js';

export default Ember.Route.extend(ApplicationRouteMixin, {
  headData: Ember.inject.service(),
  transitionHistory: [],
  transitioningToBack: false,

  
  afterModel(model) {
    this.setHeadDefaults();
  },

  setHeadDefaults() {
    // Set Global Head Defaults
    this.set('headData.siteName', 'Benevolent Tech');
    this.set('headData.twitterHandle', '@benevolentweb');
    this.set('headData.title', 'Benevolent Tech | Premium Cloud App Development');
    this.set('headData.description', 'Premium Cloud App Development');
    this.set('headData.image', 'https://benevolent.tech/wp-content/themes/benev/src/img/logos/logo-red.svg');
  },

  resetDropdownsAndOffcanvas: function() {
    // This isn't working in uikit3
    UIkit.offcanvas($('.uk-offcanvas'), 'hide');
    UIkit.dropdown($('.uk-dropdown'), 'hide');
  },

  actions: {
    // Internal Hook Actions

    // Hook entering a route. This guy is unpredictable. He doesn't fire every transition.
    // He's useful for scheduling javascript be run on page load though.
    didTransition: function() {
      // Ember.run.scheduleOnce('afterRender', this, function() {
      // });

      // Ensure the page is at top
      if (document.body.scrollTop) {
        window.scrollTo(0, 0);
      }

      // Send Google Analytics Pageview if enabled.  You must enable the google snippet in index.html.
      // ga('send', 'pageview', {'page': window.location.pathname, 'title': window.location.pathname});

      // Send Piwik Analytics Pageview if enabled.  You must enable the piwik snippet in index.html
      // Documentation: https://piwik.org/docs/custom-variables/
      // _paq.push(['trackPageView']);
    },

    // Hook exiting a route.  Very reliable.
    willTransition: function(transition) {

      this.setHeadDefaults();

      this.resetDropdownsAndOffcanvas();

      // Push history if not going backwards.
      if (!this.get('transitioningToBack')) {
        this.get('transitionHistory').push(window.location.pathname);
      }
      this.set('transitioningToBack', false);

      // Reset keydownlistener
      if (this.get('keyDownListener')) {
        this.get('keyDownListener').off();
      }
      
    },

    // Custom actions.  Note that these may be called from any child route,

    // and that all routes are children of application
    back: function() {
      var last = this.get('transitionHistory').pop();
      last = last ? last : 'layout-default.dash';
      this.set('transitioningToBack', true);
      this.transitionTo(last);
    },

    startIndexArrowListener: function(ignoreKey) {
      var self = this;

      var keyDownListener = $(document).on('keydown', function(e) {
        if (e.keyCode !== ignoreKey) {
          switch (e.keyCode) {
            case 37:
              // alert('left');
              self.transitionTo('layout-info.connect');
              break;
            case 38:
              // alert('up');
              self.transitionTo('layout-info.index');
              break;
            case 39:
              // alert('right');
              self.transitionTo('layout-info.benefit1');
              break;
            case 40:
              // alert('down');
              self.transitionTo('layout-info.benefit2');
              break;
          }
        }
      });
      this.set('keyDownListener', keyDownListener);
    }
  }
});