import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    step0() {
      var self = this;

      $('.brick0').css('opacity', 1);

      btScratchPadify('scratchpad0', 'scratch me!', 14, '#322B2A', '#000', 50, 0.95, function() {
        console.log("Step 0 done.");
        self.send('step1');
      });
    },
    step1() {
      var self = this;

      $('.brick1').css('opacity', 1);

      btScratchPadify('scratchpad1', 'now me...', 14, '#322B2A', '#000', 50, 0.95, function() {
        console.log("Step 1 done.");
        self.send('step2');
      });
    },
    step2() {
      var self = this;

      $('.brick2').css('opacity', 1);

      btScratchPadify('scratchpad2', 'and last, me!', 14, '#322B2A', '#000', 50, 0.95, function() {
        console.log("Step 2 done.");
        self.send('showContinue');
        $('#brick-bt-logo text').animate({ 'stroke-dashoffset':'0' }, 5000);
      });
    },
    initMobile() {
      var self = this;

      $('.brick3').css('opacity', 1);

      btScratchPadify('scratchpad3', 'scratch me!', 14, '#555', '#bbb', 50, 0.7, function() {
        console.log("Step 3 done.");
        self.send('showContinue');
        $('#brick-bt-logo-mobile text').animate({ 'stroke-dashoffset':'0' }, 5000);
      });
    },
    showContinue() {

      setTimeout(function() {
        $('.chevron-next-div').css('opacity', 1);

        var c = $('.chevron-next-div i'),
          position = 0;

        setInterval(function () {
          c.css('margin-left', (position % 3 * 15) + "px");
          position++;
        }, 1000);

      }, 3000);
    },

    didTransition: function() {
      $('.chevron-next-div').css('opacity', 0);
      
      Ember.run.scheduleOnce('afterRender', this, function() {
        var self = this;

        setTimeout(function() {
          self.send('step0');
          self.send('initMobile');
        }, 1000);
        
      });
    }

    
  }
});
