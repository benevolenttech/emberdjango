import Ember from 'ember';

export default Ember.Route.extend({
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', model.get('title'));
    this.set('headData.description', model.get('excerpt'));
  },

  model(params) {
    return this.get('store').findRecord('article', params.id);
  }
});