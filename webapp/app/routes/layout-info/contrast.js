import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    didTransition: function () {
      Ember.run.scheduleOnce('afterRender', this, function () {
        Ember.$('.chalkboard-background h1').textillate({initialDelay: 30, in: {effect: 'fadeInRight', delay: 200, shuffle: false}, type: 'word'});
      });
    },
    willTransition: function () {
      var self = this;
      setTimeout(function () {
        this.controller.set('colorChoice', null);
        setTimeout(function () {
          self.controller.set('showHintColumn', false);
          self.controller.set('showSuccessHint', false);
          self.controller.set('showErrorHint', false);
          Ember.$('.radio-wrapper').removeClass('clear-transform');
          Ember.$('.color-choice-div').removeClass('clear-transform');
        }, 520);
      }, 100);
    }
  }
});
