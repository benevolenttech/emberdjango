import Ember from 'ember';

export default Ember.Route.extend({
  actions: {

    didTransition: function() {
      Ember.run.scheduleOnce('afterRender', this, function() {
        var c = $('.chevron-next-div i'),
          position = 0;

        $('.chevron-next-div:not("down")').css('opacity', 0);
        $('.chevron-next-div.down').css('opacity', 1);

        this.send('startIndexArrowListener', 39);

        setInterval(function() {
          c.css('margin-top', (position % 3 * 15) + "px");
          position++;
        }, 1000);
      });
    }

  }
});
