import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    didTransition: function() {
      Ember.run.scheduleOnce('afterRender', this, function() {
        var c = $('.chevron-next-div i'),
          position = 0;

        $('.chevron-next-div:not("left")').css('opacity', 0);
        $('.chevron-next-div.left').css('opacity', 1);

        this.send('startIndexArrowListener', 40);

        setInterval(function() {
          c.css('margin-right', (position % 3 * 15) + "px");
          position++;
        }, 1000);
      });
    }

  }
});
