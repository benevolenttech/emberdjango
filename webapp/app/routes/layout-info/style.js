import Ember from 'ember';

export default Ember.Route.extend({
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Style Guide");
  },
});