import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    didTransition: function () {
      Ember.run.scheduleOnce('afterRender', this, function () {
        this.send('startIndexArrowListener', 37);
      });
    }
  }
});
