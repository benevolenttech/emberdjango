import Ember from 'ember';

export default Ember.Route.extend({
  headData: Ember.inject.service(),

  afterModel() { // model is an arg, but not using it
    this.set('headData.title', "Blog");
    this.set('headData.description', "A collection of tips and reflections on cloud software and strategy. Made with <3 from the BT team.");
  },

  model() {
    return this.get('store').findAll('article');
  }
});