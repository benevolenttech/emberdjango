import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Todo | " + model.get('task'));
  },

  model(params) {
    // console.log(params);
    if (params.id === 'add-urgent') {
      return this.get('store').createRecord('todo', {is_urgent: true});
    } else if (params.id === 'add-upcoming') {
      return this.get('store').createRecord('todo', {is_urgent: false});
    }
    return this.get('store').findRecord('todo', params.id);
  },

  actions: {
    didTransition: function() {

      // Init WYSIWYG and select title field
      Ember.run.scheduleOnce('afterRender', this, function() {
        $('.bt-trumbowyg').trumbowyg({
          // btns: ['strong', 'em', '|', 'insertImage'],
          autogrow: true
        });

        $('.todo-task').first().focus();
        setTimeout(function() { $('.todo-task').first().focus(); }, 600);
        setTimeout(function() { $('.todo-task').first().focus(); }, 1000);
        
      });

    },

    saveAndBack: function(todo) {
      var self = this;
      if (this.controller.get('complete')) {
        todo.set('completed', moment()._d);
      }
      if (this.controller.get('delete')) {
        todo.deleteRecord();
      }
      todo.save().then(function() {
        self.send('back');
      });
    },

    toggleMarkFinished: function(todo) {
      this.controller.set('delete', false);
      $('#deleteButton').css('color', 'inherit');

      if (this.controller.get('complete')) {
        this.controller.set('complete', false);
        $('#completeButton').css('color', 'inherit');
      } else {
        this.controller.set('complete', true);
        $('#completeButton').css('color', 'green');
      }
    },

    toggleMarkDeleted: function(todo) {
      this.controller.set('complete', false);
      $('#completeButton').css('color', 'inherit');

      if (this.controller.get('delete')) {
        this.controller.set('delete', false);
        $('#deleteButton').css('color', 'inherit');
      } else {
        this.controller.set('delete', true);
        $('#deleteButton').css('color', '#EE395B');
      }

    },

    todoBack: function() {
      this.controller.get('model').deleteRecord();
      this.send('back');
    }
  }
});