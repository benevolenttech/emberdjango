import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';
import ENV from 'bennyt-ember/config/environment';

export default Ember.Route.extend(UnauthenticatedRouteMixin, {
  /* Hint: Register action is in the login controller... */

  brandLogoUrl: ENV.brandLogoUrl,
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Benevolent Tech | Register");
  },

  actions: {
    // Hook entering a route.
    didTransition: function () {
      Ember.run.scheduleOnce('afterRender', this, function() {
        $('.register-form input').first().focus();
        setTimeout(function() { $('.register-form input').first().focus(); }, 600);
        setTimeout(function() { $('.register-form input').first().focus(); }, 1000);
      });
    },
  }

});