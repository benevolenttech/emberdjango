import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Benevolent Tech | Registration Confirmation");
  },
});
