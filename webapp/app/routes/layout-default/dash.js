import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  // currentUser: Ember.inject.service('current-user'),
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Dashboard");
  },

  model() {
    // return this.get('currentUser').getCurrentUser();
    return this.get('store').findAll('todo').then(function(todos) {
      var todosFinished = [],
        todosUrgent = [],
        todosUpcoming = [];
      
      todos.sortBy('sort').forEach(function(todo) {
        if (todo.get('completed')) {
          todosFinished.push(todo);
        } else if (todo.get('is_urgent')) {
          todosUrgent.push(todo);
        } else {
          todosUpcoming.push(todo);
        }
      });
      
      return {
        todosFinished: todosFinished,
        todosUrgent: todosUrgent,
        todosUpcoming: todosUpcoming
      };
      
    });
  },

  actions: {

    didTransition: function() {

      // Setup event listeners
      Ember.run.scheduleOnce('afterRender', this, function () {
        console.log("Running dash.didTransition");
        var self = this;

        $('.uk-list').on('stop', function() {
          self.send('handleDrag');
        });

        setTimeout(function() {
          self.controller.set('todosUrgentCount', self.controller.get('model.todosUrgent').length);
          self.controller.set('todosUpcomingCount', self.controller.get('model.todosUpcoming').length);
          self.controller.set('todosFinishedCount', self.controller.get('model.todosFinished').length);
          self.controller.set('updateChart', !self.controller.get('updateChart'));
        },800);
      });
      
    },

    todoMarkFinished: function(todo) {
      todo.set('completed', moment()._d);
      todo.save();
      $("li[todo-id='"+todo.get('id')+"']").remove();

      if (todo.is_urgent) {
        this.controller.set('todosUrgentCount', this.controller.get('todosUrgentCount')-1);
      } else {
        this.controller.set('todosUpcomingCount', this.controller.get('todosUpcomingCount')-1);
      }
      this.controller.set('todosFinishedCount', this.controller.get('todosFinishedCount')+1);
      this.controller.set('updateChart', !this.controller.get('updateChart'));
    },

    todoMarkDeleted: function(todo) {
      $("li[todo-id='"+todo.get('id')+"']").remove();

      if (todo.is_urgent) {
        this.controller.set('todosUrgentCount', this.controller.get('todosUrgentCount')-1);
      } else {
        this.controller.set('todosUpcomingCount', this.controller.get('todosUpcomingCount')-1);
      }
      this.controller.set('updateChart', !this.controller.get('updateChart'));

      todo.destroyRecord();
    },

    handleDrag: function() {
      var self = this,
        todosUrgentCount = 0,
        todosUpcomingCount = 0;

      console.log('something dragged.');

      $('#todosUrgentColumn li').each(function(i, e) {
        var todo = self.store.peekRecord('todo', $(this).attr('todo-id'));
        if (todo.get('sort') !== i || todo.get('is_urgent') !== true) {
          todo.set('sort', i);
          todo.set('is_urgent', true);
          todo.save();
        }
        todosUrgentCount++;
      });
      $('#todosUpcomingColumn li').each(function(i, e) {
        var todo = self.store.peekRecord('todo', $(this).attr('todo-id'));
        if (todo.get('sort') !== i || todo.get('is_urgent') !== false) {
          todo.set('sort', i);
          todo.set('is_urgent', false);
          todo.save();
        }
        todosUpcomingCount++;
      });

      this.controller.set('todosUrgentCount', todosUrgentCount);
      this.controller.set('todosUpcomingCount', todosUpcomingCount);
      this.controller.set('updateChart', !this.controller.get('updateChart'));
    },
    
    
  }

});
