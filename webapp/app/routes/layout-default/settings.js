import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  currentUser: Ember.inject.service('current-user'),
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Account Settings");
  },

  model() {
    return this.get('currentUser').getCurrentUser();
  }
});
