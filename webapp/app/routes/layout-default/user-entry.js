import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "User | " + model.get('first_name') + model.get('last_name'));
  },

  model(params) {
    // console.log(params);
    return this.get('store').findRecord('user', params.id);
  }
});
