import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  headData: Ember.inject.service(),

  afterModel(model) {
    this.set('headData.title', "Users");
  },

  model() {
    return this.get('store').findAll('user');
  }
});
