import Ember from 'ember';

/*

This service makes it easy and pretty to display validation errors
in forms based on jsonapi errors.  To do so, you must follow template and
controller conventions.

See login and register for good examples on convention.

*/

export default Ember.Service.extend({
  handleErrors: function(errors) {
    var unhandledErrors = {};

    $('.bt-field *').removeClass("uk-form-danger");
    $('.bt-field-error').html('');

    // If in jsonapi format, convert to generic format
    if ('errors' in errors) {
      var errors2 = {};
      errors.errors.forEach((error) => {
        var field = error.source.pointer.replace('/data/attributes/', '');
        errors2[field] = error.detail;
      });
      errors = errors2;
    }

    $.each(errors, (fieldName, error) => {
      if ($('#' + fieldName + '-field').length) {
        $('#' + fieldName + '-field .bt-field *').addClass("uk-form-danger");
        $('#' + fieldName + '-field .bt-field-error').html(error);
      } else {
        unhandledErrors[fieldName] = error;
      }
    });

    return unhandledErrors;
  }
});
