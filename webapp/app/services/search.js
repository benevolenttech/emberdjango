import Ember from 'ember';

export default Ember.Service.extend({
  store: Ember.inject.service('store'),

  init() {
    this.set('todos', this.get('store').findAll('todo'));
  },

  searchTodos: function(query, returnAllIfEmpty) {
    var todosFiltered = [];

    if (!query) {
      if (returnAllIfEmpty) {
        return this.get('todos');
      } else {
        return [];
      }
    }

    // Could do queryRecords here, but this is better because it will search localstore first
    query = query.toLowerCase();
    this.get('store').peekAll('todo').forEach(function(todo) {
      if (todo.get('task').toLowerCase().search(query) !== -1) {
        todosFiltered.push(todo);
      }
    });
    return todosFiltered;
  }
});
