import Ember from 'ember';
import ENV from 'bennyt-ember/config/environment';

export default Ember.Service.extend({
  session: Ember.inject.service('session'),
  store: Ember.inject.service('store'),
  
  currentUserId: "",

  getGravatarUrlFromEmail: function(email) {
    return "https://www.gravatar.com/avatar/" + md5(email.toLowerCase());
  },

  getGravatarUrl: function() {
    var self = this;
    return this.getCurrentUser().then(function (user) {
      return self.getGravatarUrlFromEmail(user.get('email'));
    });
  },
  
  getCurrentUser: function() {
    var self = this;
    
    if (!self.get('currentUserId')) {

      return Ember.$.ajax({
        url: ENV.apiHost + '/users/current/',
        type: 'GET',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        headers: {
          'Authorization': 'Token ' + self.get('session.data').authenticated.token
        }
      }).then((response) => {
        self.set('currentUserId', response.data.id);
        self.get('store').pushPayload(response);
        return self.get('store').peekRecord('user', response.data.id);
      });

    }
    else {
      // return DS.PromiseObject.create({
      //   promise: self.get('store').peekRecord('user', self.get('currentUserId'))
      // });
      return self.get('store').findRecord('user', self.get('currentUserId'));
    }
  },
  
});
