import Ember from 'ember';


export default Ember.Controller.extend({
  search: Ember.inject.service('search'),

  initTodoChart1: function() {
    var canvas = document.getElementById('todoChart1'),
      ctx = canvas.getContext('2d'),
      startingData = {
        labels: [
          "Urgent",
          "Upcoming",
          "Finished"
        ],
        datasets: [
          {
            data: [
              this.get('todosUrgentCount'),
              this.get('todosUpcomingCount'),
              this.get('todosFinishedCount')
            ],
            backgroundColor: [
              "#D18177",
              "#36A2EB",
              "#4BC0C0"
            ],
            hoverBackgroundColor: [
              "#B06055",
              "#36A2EB",
              "#19A0A0"
            ]
          }]
      },
      todoChart1 = new Chart(ctx, {type: "pie", data: startingData});

    this.set('todoChart1', todoChart1);
    return todoChart1;
  },

  updateChartAction: Ember.observer('updateChart', function() {
    var todoChart1 = $('.chartjs-hidden-iframe').length ? this.get('todoChart1') : this.initTodoChart1(),
      dataNext = [
        this.get('todosUrgentCount'),
        this.get('todosUpcomingCount'),
        this.get('todosFinishedCount')
      ];

    todoChart1.data.datasets[0].data = dataNext;
    todoChart1.update();

  }),

  searchResults: Ember.computed('searchQuery', function() {
    return this.get('search').searchTodos(this.get('searchQuery'));
  }),

});
