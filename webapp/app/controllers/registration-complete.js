import Ember from 'ember';
import ENV from 'bennyt-ember/config/environment';

export default Ember.Controller.extend({
  brandLogoUrl: ENV.brandLogoUrl,
});
