import Ember from 'ember';

export default Ember.Controller.extend({
  currentRoute: Ember.computed('currentRouteName', function() {
     return this.currentRouteName.replace('.', '-');
  })
});