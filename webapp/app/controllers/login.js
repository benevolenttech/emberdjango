import Ember from 'ember';
import ENV from 'bennyt-ember/config/environment';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  bt_forms: Ember.inject.service('bt-forms'),

  brandLogoUrl: ENV.brandLogoUrl,
  username: "demo0",
  password: "password",

  handleErrors: Ember.observer('errors', function() {
    var unhandledErrors = this.get('bt_forms.handleErrors')(this.get('errors'));
    this.set('unhandledErrors', unhandledErrors);
    this.set('unhandledErrorStr', Object.values(unhandledErrors).join(', '));
  }),

  actions: {
    authenticate() {
      let { username, password } = this.getProperties('username', 'password');
      this.get('session').authenticate('authenticator:drf-token-authenticator', username, password).catch((errorsJson) => {
        var errors = JSON.parse(errorsJson);
        this.set('errors', errors);
      });
    }
  }
});