import Ember from 'ember';

export default Ember.Controller.extend({
  colorChoiceHandler: Ember.observer('colorChoice', function() {
    var self = this,
      delayToShowHint = 0,
      isCorrect = this.get('colorChoice') === 'purple';
    
    this.set('showSuccessHint', false);
    this.set('showErrorHint', false);
    
    if (!Ember.$('.color-choice-div').hasClass('clear-transform')) {
      Ember.$('.radio-wrapper').addClass('clear-transform');
      Ember.$('.color-choice-div').addClass('clear-transform');
      delayToShowHint = 500;  
    }
    
    setTimeout(function() {
      self.set('showHintColumn', true);
      self.set('showSuccessHint', isCorrect);
      self.set('showErrorHint', !isCorrect);

      if (isCorrect) {
        Ember.$.confetti.restart();
        setTimeout(function() {
          Ember.$.confetti.stop();
        }, 2000);
      }
    }, delayToShowHint);

  })
});
