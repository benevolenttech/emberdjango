import Ember from 'ember';
import ENV from 'bennyt-ember/config/environment';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  bt_forms: Ember.inject.service('bt-forms'),

  brandLogoUrl: ENV.brandLogoUrl,

  handleErrors: Ember.observer('errors', function() {
    var unhandledErrors = this.get('bt_forms.handleErrors')(this.get('errors'));
    this.set('unhandledErrors', unhandledErrors);
    this.set('unhandledErrorStr', Object.values(unhandledErrors).join(', '));
  }),

  actions: {
    register() {
      let {first_name, last_name, username, password} = this.getProperties(
        'first_name',
        'last_name',
        'username',
        'password'
      );

      // TODO: Validate form

      Ember.$.ajax({
        url: ENV.apiHost + '/users/',
        type: 'POST',
        data: JSON.stringify({
          "data": {
            "type": "User",
            "id": "1",
            "attributes": {
              "username": username,
              "email": username,
              "first-name": first_name,
              "last-name": last_name,
              "password": password
            }
          }
        }),
        contentType: 'application/vnd.api+json;charset=utf-8',
        dataType: 'json'
      }).then((response) => {
        this.set('signupComplete', true);

        let {username, password} = this.getProperties('username', 'password');
        this.get('session').authenticate('authenticator:drf-token-authenticator', username, password)
        .then(() => {
          this.transitionToRoute('registration-complete');
        })
        .catch((errorsJson) => {
          var errors = JSON.parse(errorsJson);
          this.set('errors', errors);
        });

      }, (xhr, status, error) => {
        var errors = JSON.parse(xhr.responseText);
        this.set('errors', errors);
      });
    }
  }
});