import DS from 'ember-data';

export default DS.Model.extend({
  username: DS.attr(),
  first_name: DS.attr(),
  last_name: DS.attr(),
  email: DS.attr(),
  is_staff: DS.attr(),
  articles: DS.hasMany('article'),
  todos: DS.hasMany('todo')
});
