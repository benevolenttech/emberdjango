import DS from 'ember-data';

export default DS.Model.extend({
  task: DS.attr(),
  notes: DS.attr(),
  owner: DS.belongsTo('user'),
  is_urgent: DS.attr('boolean'),
  sort: DS.attr('number'),
  completed: DS.attr('date')
});
