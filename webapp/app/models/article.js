import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr(),
  pub_date: DS.attr('date'),
  body: DS.attr(),
  excerpt: DS.attr(),
  author: DS.belongsTo('user')
});
