import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  // Per-Route Layouts
  this.route('login');
  this.route('register');
  this.route('password-reset');
  this.route('registration-complete');
  this.route('todo-entry', { path: "todos/:id" });

  // Default App Layout
  this.route('layout-default', {path: 'user'}, function() {
    this.route('dash', {path: '/'});
    this.route('settings');
    this.route('user-list', { path: "users" });
    this.route('user-entry', { path: "users/:id" });
  });

  // Default Info Layout
  this.route('layout-info', {path: '/'}, function() {
    this.route('index', {path: '/'});
    this.route('article-list', { path: "articles" });
    this.route('article-entry', { path: "articles/:id" });
    this.route('style');
    this.route('terms-of-service');
    this.route('privacy-policy');
    this.route('404', {path: '*path'});
    this.route('benefit1');
    this.route('benefit2');
    this.route('connect');
    this.route('about-us');
    this.route('contrast');
    this.route('funnel');
  });
});

export default Router;
