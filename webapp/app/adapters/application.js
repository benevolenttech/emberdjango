import DS from 'ember-data';
import ENV from 'bennyt-ember/config/environment';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';

export default DS.JSONAPIAdapter.extend(DataAdapterMixin, {
  host: ENV.apiHost,

  authorizer: 'authorizer:drf-token-authorizer',

  buildURL() {
    // add trailing slash and format query, because django rest expects it
    return this._super(...arguments) + '/';
  },

  // Additional options
  // headers: {
  //   'MY_CUSTOM_HEADER': 'application/json'
  // },
  // namespace: 'api',
  // pathForType(type) {
  //   let entityPath;
  //   switch(type) {
  //     case 'article':
  //       entityPath = 'node/article';
  //       break;
  //     case 'node--page':
  //       entityPath = 'node/page';
  //       break;
  //     case 'user--user':
  //       entityPath = 'user/user';
  //       break;
  //   }
  //   return entityPath;
  // },
});
