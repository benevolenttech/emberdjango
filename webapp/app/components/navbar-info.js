import Ember from 'ember';
import ENV from 'bennyt-ember/config/environment';

export default Ember.Component.extend({
  session: Ember.inject.service('session'),
  currentUser: Ember.inject.service('current-user'),

  brandLogoUrl: ENV.brandLogoUrl,

  // gravatarUrl: Ember.computed('session.isAuthenticated', function() {
  //   var self = this;
  //   this.get('currentUser').getGravatarUrl().then(function(url) {
  //     self.set('gravatarUrl', url);
  //   });
  //   this.get('currentUser').getCurrentUser().then(function(user) {
  //     self.set('userGreeting', user.get('first_name'));
  //   });
  // }),

  isAuthenticated: Ember.computed('session.isAuthenticated', function() {
    return this.get('session').get('isAuthenticated');
  }),


  actions: {
    invalidateSession() {
      this.get('session').invalidate();
    },
  }
});
