import Ember from 'ember';
import ENV from 'bennyt-ember/config/environment';

export default Ember.Component.extend({
  session: Ember.inject.service('session'),
  search: Ember.inject.service('search'),
  currentUser: Ember.inject.service('current-user'),

  init() {
    var self = this;
    this._super();
    Ember.run.scheduleOnce('afterRender', this, function () {
      $('#search-offcanvas').on('show', function() {
        $('#field-search').focus();
      });

    });
  },

  brandLogoUrl: ENV.brandLogoUrl,

  // gravatarUrl: Ember.computed('session.isAuthenticated', function() {
  //   var self = this;
  //   this.get('currentUser').getGravatarUrl().then(function(url) {
  //     self.set('gravatarUrl', url);
  //   });
  //   this.get('currentUser').getCurrentUser().then(function(user) {
  //     self.set('userGreeting', user.get('first_name'));
  //   });
  // }),

  isAuthenticated: Ember.computed('session.isAuthenticated', function() {
    return this.get('session').get('isAuthenticated');
  }),

  todos: Ember.computed('searchQuery', 'session.isAuthenticated', function() {
    if (this.get('isAuthenticated')) {
      return this.get('search').searchTodos(this.get('searchQuery'), true);
    }
  }),


  actions: {
    invalidateSession() {
      this.get('session').invalidate();
    },
  }
});
