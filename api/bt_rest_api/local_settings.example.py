

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ta$nc2&qngg4e(ye-nu#i0ms2d(7waa^%kcms1w&2c#9-%s#+0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': '127.0.0.1',
        'NAME': 'framework',
        'USER': 'framework2',
        'PASSWORD': 'uH9f9i2?',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost',
    '.benevolent.tech',
    '.benev.tech'
]


# For more options check https://github.com/ottoyiu/django-cors-headers/
# CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    'localhost:4200',
    '127.0.0.1:4200',
    # 'framework.benev.tech'
)
CORS_ORIGIN_REGEX_WHITELIST = (
    # r'^(http?://)?(\w+\.)?localhost:4200$',
    # r'^(http?://)?(\w+\.)?127\.0\.0\.1:4200$',
    r'^(https?://)?(\w+\.)?benev\.tech$',
)
# CORS_REPLACE_HTTPS_REFERER = True
# CORS_ALLOW_CREDENTIALS = True
# from corsheaders.defaults import default_headers
# CORS_ALLOW_HEADERS = default_headers + (
#     'my_custom_header',
# )