# Django Rest API Boilerplate #

______________________


## Getting started: ##

1. Install pip using homebrew
1. Install virtualenvironmentwrapper - http://virtualenvwrapper.readthedocs.io/en/latest/
1. Create a global environment folder (I recommend ~/env)
1. Create an environment and load it
1. Install dependences
1. pip install django
1. pip install djangorestframework
1. pip install markdown
1. pip install django-filter
1. Clone this repot
1. Create PostGres db with settings bt_rest_api/local_settings.py 
1. python manage.py migrate


## Deploying: ##

https://support.plesk.com/hc/en-us/articles/115002701209-How-to-configure-Django-application-in-Plesk-

## Moving the base url ##

If moving the base url, you'll need to adjust bt_rest_api/local_settings.py

## CORS Headers ##

Right now, all domains are accepted. To restrict, see bt_rest_api/local_settings.py.

Follow the instructions in the references.dd
References:

1. http://www.django-rest-framework.org/
1. http://django-rest-framework-json-api.readthedocs.io/en/stable/usage.html
1. http://virtualenvwrapper.readthedocs.io/en/latest/
1. http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-django.html
