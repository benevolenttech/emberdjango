from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model

from bt_rest_api_app.endpoints.todos import UrgentTodoFactory, UpcomingTodoFactory, FinishedTodoFactory
from bt_rest_api_app.endpoints.articles import ArticleFactory


class Command(BaseCommand):
    help = 'Creates a bunch of demo data'

    def handle(self, *args, **options):
        User = get_user_model()
        u = User.objects.first()

        UrgentTodoFactory(owner=u).save()
        UrgentTodoFactory(owner=u).save()
        UrgentTodoFactory(owner=u).save()
        UpcomingTodoFactory(owner=u).save()
        UpcomingTodoFactory(owner=u).save()
        UpcomingTodoFactory(owner=u).save()
        UpcomingTodoFactory(owner=u).save()
        FinishedTodoFactory(owner=u).save()

        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()
        ArticleFactory(author=u).save()

        self.stdout.write(self.style.SUCCESS('Successfully created demo data'))