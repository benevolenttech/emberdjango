from django.core.management.base import BaseCommand, CommandError

from bt_rest_api_app.endpoints.users import UserFactory, StaffFactory, AdminFactory
from bt_rest_api_app.endpoints.todos import TodoFactory, UrgentTodoFactory
from bt_rest_api_app.endpoints.articles import ArticleFactory


class Command(BaseCommand):
    help = 'Creates a bunch of demo data'

    def handle(self, *args, **options):
        a = AdminFactory()
        a.save()
        s = StaffFactory()
        s.save()
        u = UserFactory()
        u.save()

        self.stdout.write(self.style.SUCCESS('Successfully created demo users'))