from __future__ import unicode_literals

from django.contrib.auth.models import User

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import permissions, viewsets
from rest_framework.response import Response
from rest_framework_json_api import serializers, relations

from oauth2_provider.ext.rest_framework import IsAuthenticatedOrTokenHasScope, TokenHasReadWriteScope, TokenHasScope

from dry_rest_permissions.generics import DRYPermissions
from dry_rest_permissions.generics import unauthenticated_users, allow_staff_or_superuser, authenticated_users
from dry_rest_permissions.generics import DRYPermissionFiltersBase

import factory


'''
Couldn't get DRYPermissions working on the user model :-(

'''


# Serializers define the API representation.
class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    # username = serializers.CharField(source='email')
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)

    class Meta:
        resource_name = "users"
        model = User
        fields = ('url', 'username', 'email', 'is_staff', 'password', 'first_name', 'last_name')

    def create(self, validated_data):
        user = User()
        user.set_password(validated_data['password'])
        validated_data['password'] = user.password
        return super(UserSerializer, self).create(validated_data)

    # def get(self):
    #     user = User()
    #     user.set_password(validated_data['password'])
    #     validated_data['password'] = user.password
    #     return super(UserSerializer, self).create(validated_data)

    # TODO: Create reset password endpoint


# Limits all list requests to only be seen by the owners or creators.
class UserFilterBackend(DRYPermissionFiltersBase):
    def filter_queryset(self, request, queryset, view):
        if request.user.is_superuser:
            return queryset.filter()
        else:
            return queryset.filter(id=request.user.id)

    def filter_list_queryset(self, request, queryset, view):
        if request.user.is_superuser:
            return queryset.filter()
        else:
            return queryset.filter(id=request.user.id)


class IsStaffOrTargetUser(permissions.BasePermission):
    def has_permission(self, request, view):
        # allow user to list all users if logged in user is staff
        return view.action == 'retrieve' or request.user.is_staff

    def has_object_permission(self, request, view, obj):
        # allow logged in user to view own details, allows staff to view all records
        return request.user.is_staff or obj == request.user


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticatedOrTokenHasScope]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (UserFilterBackend, DjangoFilterBackend)

    def get_permissions(self):
        # allow non-authenticated user to create via POST
        return (permissions.AllowAny() if self.request.method == 'POST' else IsStaffOrTargetUser(),)

    def retrieve(self, request, *args, **kwargs):
        if kwargs['pk'] == 'current' and request.user.is_authenticated:
            serializer = self.get_serializer(request.user)
            return Response(serializer.data)
        return super(UserViewSet, self).retrieve(request, *args, **kwargs)


class UserFactory(factory.Factory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'demo{0}'.format(n))
    email = factory.Sequence(lambda n: 'demo{0}@demo.com'.format(n))
    password = factory.PostGenerationMethodCall('set_password', 'password')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')


class StaffFactory(UserFactory):
    is_staff = True


class AdminFactory(UserFactory):
    is_staff = True
    is_superuser = True

