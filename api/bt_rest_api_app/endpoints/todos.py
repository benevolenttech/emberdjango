from __future__ import unicode_literals

from random import randint
from faker import Faker
fake = Faker()

from django.conf import settings
from django.db import models
from django.utils import timezone

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import permissions, viewsets
from rest_framework_json_api import serializers, relations

# from oauth2_provider.ext.rest_framework import IsAuthenticatedOrTokenHasScope, TokenHasReadWriteScope, TokenHasScope

from dry_rest_permissions.generics import DRYPermissions
from dry_rest_permissions.generics import allow_staff_or_superuser, authenticated_users
from dry_rest_permissions.generics import DRYPermissionFiltersBase

import factory


class Todo(models.Model):
    task = models.CharField(max_length=300)
    last_modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)
    is_urgent = models.BooleanField(default=False)
    sort = models.PositiveIntegerField(default=0)
    notes = models.TextField(null=True)
    completed = models.DateTimeField(null=True)

    @staticmethod
    @authenticated_users  # return false for anonymous
    def has_read_permission(request):
        return True

    def has_object_read_permission(self, request):
        return True if request.user.is_superuser else request.user == self.owner

    @staticmethod
    @authenticated_users  # return false for anonymous
    def has_write_permission(request):
        return True

    def has_object_write_permission(self, request):
        return True if request.user.is_superuser else request.user == self.owner


# Serializers define the API representation.
class TodoSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(default=serializers.CurrentUserDefault(), read_only=True)

    class Meta:
        model = Todo
        # resource_name = "todos" # can override the resource type field if so desired
        fields = ('task', 'owner', 'last_modified', 'created', 'is_urgent', 'sort', 'notes', 'completed')


# Limits all list requests to only be seen by the owners or creators.
class TodoFilterBackend(DRYPermissionFiltersBase):
    def filter_list_queryset(self, request, queryset, view):
        if request.user.is_superuser:
            return queryset.filter()
        else:
            return queryset.filter(owner=request.user)


# ViewSets define the view behavior.
class TodoViewSet(viewsets.ModelViewSet):
    # permission_classes = [IsAuthenticatedOrTokenHasScope]
    permission_classes = (DRYPermissions,)
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    filter_fields = ('task', 'id', 'owner', 'last_modified', 'created', 'is_urgent', 'sort', 'notes', 'completed')
    filter_backends = (TodoFilterBackend, DjangoFilterBackend)


class TodoFactory(factory.Factory):
    class Meta:
        model = Todo

    owner = factory.SubFactory('bt_rest_api_app.endpoints.users.UserFactory')
    # task = factory.Faker('sentence', nb_words=randint(4, 7)) # doesn't lazy randint
    task = factory.LazyAttribute(lambda a: fake.sentence(nb_words=randint(4, 7)))
    sort = factory.LazyAttribute(lambda a: randint(0, 7))
    notes = factory.LazyAttribute(lambda a: fake.text() + fake.text() + "\n<br><br>\n" + fake.text() + fake.text() + fake.text() + "\n<br><br>\n" + fake.text() + fake.text())
    is_urgent = factory.Faker('boolean')
    completed = factory.LazyAttribute(lambda a: fake.date() if randint(0,1) else None)


class UrgentTodoFactory(TodoFactory):
    is_urgent = True
    completed = None


class UpcomingTodoFactory(TodoFactory):
    is_urgent = False
    completed = None


class FinishedTodoFactory(TodoFactory):
    completed = fake.date()