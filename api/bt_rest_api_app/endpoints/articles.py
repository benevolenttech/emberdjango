from __future__ import unicode_literals

from random import randint
from faker import Faker
fake = Faker()

from django.conf import settings
from django.db import models
from django.utils import timezone

from rest_framework import permissions, viewsets
from rest_framework_json_api import serializers, relations

# from oauth2_provider.ext.rest_framework import IsAuthenticatedOrTokenHasScope, TokenHasReadWriteScope, TokenHasScope

from dry_rest_permissions.generics import DRYPermissions
from dry_rest_permissions.generics import unauthenticated_users, allow_staff_or_superuser, authenticated_users

import factory


class Article(models.Model):
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', auto_now=True)
    last_modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    body = models.TextField()
    excerpt = models.TextField()
    author = models.ForeignKey(settings.AUTH_USER_MODEL)

    @staticmethod
    def has_read_permission(request):
        return True

    def has_object_read_permission(self, request):
        return True

    @staticmethod
    @allow_staff_or_superuser  # always returns true for them.
    def has_write_permission(request):
        return False

    def has_object_write_permission(self, request):
        return True if request.user.is_superuser else request.user == self.author


# Serializers define the API representation.
class ArticleSerializer(serializers.ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(default=serializers.CurrentUserDefault(), read_only=True)
    pub_date = serializers.DateTimeField(default=timezone.now)

    class Meta:
        model = Article
        # resource_name = "articles" # can override the resource type field if so desired
        fields = ('title', 'pub_date', 'body', 'author', 'last_modified', 'created', 'excerpt')


# ViewSets define the view behavior.
class ArticleViewSet(viewsets.ModelViewSet):
    # permission_classes = [IsOwnerOrReadOnly]
    permission_classes = (DRYPermissions,)
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    filter_fields = ('title', 'pub_date', 'id', 'author', 'last_modified', 'created')


class ArticleFactory(factory.Factory):
    class Meta:
        model = Article

    author = factory.SubFactory('bt_rest_api_app.endpoints.users.UserFactory')
    title = factory.LazyAttribute(lambda a: fake.sentence(nb_words=randint(4, 7)))
    # body = factory.Faker('text') # not long enough
    body = factory.LazyAttribute(lambda a: fake.text() + fake.text() + "\n<br><br>\n" + fake.text() + fake.text() + fake.text() + "\n<br><br>\n" + fake.text() + fake.text())
    excerpt = factory.LazyAttribute(lambda a: fake.text() + fake.text())
    pub_date = factory.Faker('date')

