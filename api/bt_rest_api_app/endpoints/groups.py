from django.contrib.auth.models import Group
from rest_framework import permissions, viewsets
from rest_framework_json_api import serializers, relations

from oauth2_provider.ext.rest_framework import IsAuthenticatedOrTokenHasScope, TokenHasReadWriteScope, TokenHasScope

# from dry_rest_permissions.generics import DRYPermissions

import factory


# Serializers define the API representation.
class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        # fields = ('url', 'username', 'email', 'is_staff')


# ViewSets define the view behavior.
class GroupViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticatedOrTokenHasScope]
    required_scopes = ['groups']
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

