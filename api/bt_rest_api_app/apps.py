from __future__ import unicode_literals

from django.apps import AppConfig


class BtRestApiAppConfig(AppConfig):
    name = 'bt_rest_api_app'
